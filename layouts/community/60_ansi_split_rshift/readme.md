# 60_ansi_split_rshift

    LAYOUT_60_ansi_split_rshift

![60% ANSI with split RSHIFT Layout Image](https://photos.app.goo.gl/bVwSvSXPHhrTGPqk9)

```
# 60% ANSI with split RSHIFT Layout

A modified layout of the default ANSI 60% layout with a split right shift.
While this is a similar layout, the shift row has a split right shift which
is used as an additional mod.  In the dz60:spr0k3t build this is used as a
toggle between the qwerty layer and the movement/media layer.
```

# DZ60

![dz60](https://cdn.shopify.com/s/files/1/1473/3902/files/1_6525343b-ee62-47e8-882a-05e316136a3f.jpg?v=1501657073)

A customizable 60% keyboard.

Keyboard Maintainer: QMK Community  
Hardware Supported: DZ60  
Hardware Availability: [kbdfans](https://kbdfans.myshopify.com/collections/pcb/products/dz60-60-pcb?variant=40971616717)

Make example for this keyboard (after setting up your build environment):

    make dz60:spr0k3t

See [build environment setup](https://docs.qmk.fm/build_environment_setup.html) then the [make instructions](https://docs.qmk.fm/make_instructions.html) for more information.
