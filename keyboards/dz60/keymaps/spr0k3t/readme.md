# Building my DZ60 with a split RSHIFT

     Keymap: (Base Layer) Default Layer
     ,-----------------------------------------------------------.
     |  ~ | 1|  2|  3|  4|  5|  6|  7|  8|  9|  0|  -|  =|Backsp |
     |-----------------------------------------------------------|
     |Tab  |  Q|  W|  E|  R|  T|  Y|  U|  I|  O|  P|  [|  ]|  \  |
     |-----------------------------------------------------------|
     |ESC/CTL|  A|  S|  D|  F|  G|  H|  J|  K|  L|  ;|  '|Return |
     |-----------------------------------------------------------|
     |Shift   |  Z|  X|  C|  V|  B|  N|  M|  ,|  .|  /|Shift| MO |
     |-----------------------------------------------------------|
     |Ctrl|Gui |Alt |         Space         |Alt |Gui |MO  |Ctrl |
     `-----------------------------------------------------------' 

I wanted a simple looking keyboard but it needed to have some extra functionality so I could use other layers of the keyboard for mods and macros.  The RSHIFT is a 1.75U size allowing for a key split for an additional 1U mod.

* Split RSHIFT Mod key works as a layer toggle activating the Movement Control and Media.
* RGUI accesses the Macro and RGB controls.
* Sample macros I've included are simple tools for VIM.  Specifically used for building configurations on Cisco VoIP phones.  Feel free to modify those as you see fit.
