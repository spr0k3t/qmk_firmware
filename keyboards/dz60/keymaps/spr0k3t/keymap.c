#include QMK_KEYBOARD_H

#define MODS_CTRL_MASK  (MOD_BIT(KC_LSHIFT)|MOD_BIT(KC_RSHIFT))
#define XXXXXXX KC_NO
#define _______ KC_TRNS

enum layers {
        _QWERTY,
        _MCMDA,
        _RGB
};

enum custom_keycodes {
        M_BLD,
        M_LINE
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/*    Keymap: (Base Layer) Default Layer
     ,-----------------------------------------------------------.
     |  ~ | 1|  2|  3|  4|  5|  6|  7|  8|  9|  0|  -|  =|Backsp |
     |-----------------------------------------------------------|
     |Tab  |  Q|  W|  E|  R|  T|  Y|  U|  I|  O|  P|  [|  ]|  \  |
     |-----------------------------------------------------------|
     |ESC/CTL|  A|  S|  D|  F|  G|  H|  J|  K|  L|  ;|  '|Return |
     |-----------------------------------------------------------|
     |Shift   |  Z|  X|  C|  V|  B|  N|  M|  ,|  .|  /|Shift|2 MO|
     |-----------------------------------------------------------|
     |Ctrl|Gui |Alt |         Space         |Alt |Gui |MO  |Ctrl |
     `-----------------------------------------------------------' */

        [_QWERTY] = LAYOUT(
                KC_GRV          , KC_1   , KC_2   , KC_3   , KC_4   , KC_5   , KC_6   , KC_7   , KC_8    , KC_9    , KC_0      , KC_MINS, KC_EQL , XXXXXXX, KC_BSPC,
                KC_TAB          , KC_Q   , KC_W   , KC_E   , KC_R   , KC_T   , KC_Y   , KC_U   , KC_I    , KC_O    , KC_P      , KC_LBRC, KC_RBRC, KC_BSLS,
                LCTL_T(KC_ESC)  , KC_A   , KC_S   , KC_D   , KC_F   , KC_G   , KC_H   , KC_J   , KC_K    , KC_L    , KC_SCLN   , KC_QUOT, KC_ENT ,
                KC_LSFT, XXXXXXX, KC_Z   , KC_X   , KC_C   , KC_V   , KC_B   , KC_N   , KC_M   , KC_COMM , KC_DOT  , KC_SLSH   , KC_RSFT, TO(_MCMDA),
                KC_LCTL, KC_LGUI, KC_LALT, KC_SPC          , KC_SPC          , KC_SPC , KC_RALT, MO(_RGB), MO(_RGB), MO(_MCMDA), KC_RCTL),

/*    Keymap: (Movement Control and Media) Fn Modifier or RMOD
     ,-----------------------------------------------------------.
     |  ~ |F1| F2| F3| F4| F5| F6| F7| F8| F9|F10|F11|F12|  DEL  |
     |-----------------------------------------------------------|
     |     |HME| UP|END|PGU|   |   |   |   |   |   |VLU|VLD| MUTE|
     |-----------------------------------------------------------|
     |CAPS LK|LFT|DWN|RHT|PGD|   |   |   |   |   |   |   |       |
     |-----------------------------------------------------------|
     |        |   |   |   |   |   |   |   |MPv|MNx|   |     | MO |
     |-----------------------------------------------------------|
     |    |    |    |   Media Play/Pause    |    |    |    |     |
     `-----------------------------------------------------------' */

        [_MCMDA] = LAYOUT(
                KC_ESC , KC_F1  , KC_F2  , KC_F3  , KC_F4  , KC_F5  , KC_F6  , KC_F7  , KC_F8  , KC_F9  , KC_F10 , KC_F11 , KC_F12 , _______, KC_DEL ,
                _______, KC_HOME, KC_UP  , KC_END , KC_PGUP, _______, _______, _______, _______, _______, _______, KC_VOLD, KC_VOLU, KC_MUTE,
                KC_CLCK, KC_LEFT, KC_DOWN, KC_RGHT, KC_PGDN, _______, _______, _______, _______, _______, _______, _______, _______,
                _______, _______, _______, _______, _______, _______, _______, _______, _______, KC_MPRV, KC_MNXT, _______, _______, TO(_QWERTY),
                _______, _______, _______, KC_MPLY         , KC_MPLY         , KC_MPLY, _______, _______, _______, _______, _______),

        [_RGB] = LAYOUT(
                _______, M_BLD  , M_LINE , M(3)   , M(4)   , M(5)   , M(6)   , M(7)   , M(8)   , M(9)   , M(10)  , M(11)  , M(12)  , _______, _______,
                _______, RGB_TOG, RGB_MOD, RGB_HUI, RGB_HUD, RGB_SAI, RGB_SAD, RGB_VAI, RGB_VAD, _______, _______, _______, _______, RESET  ,
                _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
                _______, _______, _______, _______, BL_DEC , BL_TOGG, BL_INC , BL_STEP, _______, _______, _______, _______, _______, TO(_QWERTY),
                _______, _______, _______, _______         , _______         , _______, _______, _______, _______, _______, _______),
};

enum function_id {
    SHIFT_ESC,
};

const uint16_t PROGMEM fn_actions[] = {
  [0]  = ACTION_FUNCTION(SHIFT_ESC),
};

void action_function(keyrecord_t *record, uint8_t id, uint8_t opt) {
  static uint8_t shift_esc_shift_mask;
  switch (id) {
    case SHIFT_ESC:
      shift_esc_shift_mask = get_mods()&MODS_CTRL_MASK;
      if (record->event.pressed) {
        if (shift_esc_shift_mask) {
          add_key(KC_GRV);
          send_keyboard_report();
        } else {
          add_key(KC_ESC);
          send_keyboard_report();
        }
      } else {
        if (shift_esc_shift_mask) {
          del_key(KC_GRV);
          send_keyboard_report();
        } else {
          del_key(KC_ESC);
          send_keyboard_report();
        }
      }
      break;
  }
}

//VIM Specific code for building a line with Cisco Phones
bool process_record_user(uint16_t keycode, keyrecord_t *record) {
        switch (keycode) {
                case M_BLD:
                        if (record->event.pressed) {
                                SEND_STRING(SS_TAP(X_ESCAPE)":0"SS_TAP(X_ENTER)"O<flat-profile>"SS_TAP(X_ESCAPE)":$"SS_TAP(X_ENTER)"o</flat-profile>"SS_TAP(X_ESCAPE));
                        } else {
                        }
                break;
                case M_LINE:
                        if (record->event.pressed) {
                                SEND_STRING(SS_TAP(X_ESCAPE)"j0wwye$a </ "SS_TAP(X_ESCAPE)"$Pa>"SS_TAP(X_ESCAPE));
                        } else {
                        }
                break;
        }
        return true;
};

/*bool process_record_user(uint16_t keycode, keyrecord_t *record) {
        if (record->event.pressed) {
                switch (keycode) {
                        case M_BLD:
                                SEND_STRING(SS_TAP(X_ESCAPE)":0"SS_TAP(X_ENTER)"O<flat-profile>"SS_TAP(X_ESCAPE)":$"SS_TAP(X_ENTER)"o</flat-profile>"SS_TAP(X_ESCAPE));
                                return false; break;
                        case M_LINE:
                                SEND_STRING(SS_TAP(X_ESCAPE)"j0wwye$a </ "SS_TAP(X_ESCAPE)"$Pa>"SS_TAP(X_ESCAPE));
                                return false; break;
                }
        }
        return true;
};*/

